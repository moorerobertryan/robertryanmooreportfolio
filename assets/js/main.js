/*
	Massively by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

function addChild() {
  const placeholder = document.getElementById("response");
  fetch("childForm.html")
    .then((response) => response.text())
    .then((html) => {
      placeholder.insertAdjacentHTML("beforebegin", html);
    })
    .catch((error) => console.error(error));

  //var i = document.getElementsByClassName("child-form").length;

  // document.getElementsByName("childName")[i].value;
  // document.getElementsByName("childSurname")[i].value;
  // document.getElementsByName("childBirthdate")[i].value;
  // document.getElementsByName("grade")[i].value;
  // document.getElementsByName("gender")[i].value;
  // document.getElementsByName("school")[i].value;
  // document.getElementsByName("medicalNotes")[i].value;
  // document.getElementsByName("additionalComments")[i].value;
  // document.getElementsByName("popia")[i].value;
  // document.getElementsByName("indemnity")[i].value;

  var forms = document.getElementsByClassName("child-form");

  // Get the number of forms with this class
  var i = forms.length;

  // Get the last form with class "child-form"
  var lastForm = forms[i - 1];

  // Get all the elements in the last form
  var elements = lastForm.getElementsByTagName("*");

  // Loop through all the elements and set their ids
  for (var j = 0; j < elements.length; j++) {
    var element = elements[j];
    var id = element.getAttribute("id");
    if (id !== null) {
      element.setAttribute("id", id + i);
      if (id !== null) {
        element.setAttribute("id", id + i);
        // update the "for" attribute of the associated label, if there is one
        var labels = document.getElementsByTagName("label");
        for (var k = 0; k < labels.length; k++) {
          var label = labels[k];
          if (label.getAttribute("for") === id) {
            label.setAttribute("for", id + i);
          }
        }
      }
    }
  }
}

async function register() {
  var responseMsg = '';
  const shieldContainer = document.getElementById("shield-container");
  try {
    document.getElementById("responseError").id = "response";
  } catch {}
  document.getElementById("response").textContent = responseMsg
  try {
    const parentForm = document.getElementById("parentform");
    const parentName = parentForm.elements.parentName.value;
    const email = parentForm.elements.email.value;
    const contactNumber = parentForm.elements.contactNumber.value;
    const altContactName = parentForm.elements.altContactName.value;
    const altContactNumber = parentForm.elements.altContactNumber.value;

    const childForms = document.getElementsByClassName("child-form");
    const children = [];

    for (let i = 0; i < childForms.length; i++) {
      const childForm = childForms[i];
      const childName = childForm.elements.childName.value;
      const childSurname = childForm.elements.childSurname.value;
      const childBirthdate = childForm.elements.childBirthdate.value;
      const grade = childForm.elements.grade.value;
      const gender = childForm.elements.gender.value;
      const school = childForm.elements.school.value;
      const medicalNotes = childForm.elements.medicalNotes.value;
      const additionalComments = childForm.elements.additionalComments.value;
      const popia = childForm.elements.popia.checked ? "x" : "";
      const indemnity = childForm.elements.indemnity.checked ? "x" : "";
      const child = {
        "Name": childName,
        "Surname": childSurname,
        "Birthdate": childBirthdate,
        "Grade": grade,
        "Gender": gender,
        "School": school,
        "Medical notes (allergies or conditions we should know about)": medicalNotes,
        "Any additional comments": additionalComments,
        "Popia form": popia,
        "Indemnity form": indemnity
      };
      //validation
      if (!childName || !childSurname || !childBirthdate || !grade || !gender || !school || !parentName || !email || !contactNumber) {
        document.getElementById("response").id = "responseError";
        document.getElementById("responseError").textContent =
        "All fields except medical notes and additional comments are mandatory!";
        return;
      }
      shieldContainer.style.display = "block";
      children.push(child);
    }

    const parent = {
      "Parent name": parentName,
      "Email address": email,
      "Contact number": contactNumber,
      "Alternative contact name": altContactName,
      "Alternative contact number": altContactNumber,
    };

    const result = [parent, ...children];
    const jsonResult = JSON.stringify(result);

    const response = await fetch(/*"https://hbc.robertryanmoore.co.za/api/submit_data"*/ "http://127.0.0.1:5000/api/submit_data", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: jsonResult,
    });

    if (response.ok) {
      responseMsg = "Success! You should receive a confirmation email shortly.";
    } else {
      throw new Error(response.statusText);
    }
  } catch (error) {
    console.error(error);
    document.getElementById("response").id = "responseError";
    responseMsg = "Error! Unable to submit. Please try again shortly.";
    document.getElementById("submitbutton").innerHTML = "Submit";
  } finally {
    setTimeout(() => {
      shieldContainer.style.display = "none";
      try {
        document.getElementById("response").textContent = responseMsg
      } catch (error) {
        document.getElementById("responseError").textContent = responseMsg
      }
    }, 2000);
  }
}


function submit() {
  document.getElementById("submitbutton").innerHTML = "Submitting...";
  //incase this method has been called
  try {
    document.getElementById("responseError").id = "response";
  } catch {}

  var firstName = document.getElementsByName("firstName")[0].value;
  var lastName = document.getElementsByName("lastName")[0].value;
  var email = document.getElementsByName("email")[0].value;
  var subject = document.getElementsByName("subject")[0].value;
  var message = document.getElementsByName("message")[0].value;

  var dict = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    subject: subject,
    message: message,
  };

  //TODO: Validation
  if (
    firstName === null ||
    firstName === "" ||
    lastName === null ||
    lastName == "" ||
    subject === null ||
    subject === "" ||
    message === null ||
    message === ""
  ) {
    document.getElementById("response").id = "responseError";
    document.getElementById("responseError").textContent =
      "All fields are mandatory!";
    document.getElementById("submitbutton").innerHTML = "Submit";
  } else {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      try {
        $.ajax({
          type: "POST",
          url: "https://emailer.robertryanmoore.co.za/v1/submit", //production
          //url: "http://127.0.0.1:5000/v1/submit", //localhost
          data: JSON.stringify(dict),
          contentType: "application/json",
        });
        document.getElementById("submitbutton").innerHTML = "Submitted";
        document.getElementById("submitbutton").disabled = true;
        document.getElementsByName("firstName")[0].readOnly = true;
        document.getElementsByName("lastName")[0].readOnly = true;
        document.getElementsByName("email")[0].readOnly = true;
        document.getElementsByName("subject")[0].readOnly = true;
        document.getElementsByName("message")[0].readOnly = true;
        document.getElementById("response").textContent =
          "Success! You should recieve a comfirmation email shortly.";
      } catch (error) {
        console.log(error);
        document.getElementById("response").id = "responseError";
        document.getElementById("responseError").textContent =
          "Error! Unable to sumbit. Please try again shortly.";
        document.getElementById("submitbutton").innerHTML = "Submit";
      }
    } else {
      document.getElementById("submitbutton").innerHTML = "Submit";
      document.getElementById("response").id = "responseError";
      document.getElementById("responseError").textContent =
        "You have entered an invalid email address!";
    }
  }
}

function aboutme() {
  // get the clock
  var aboutme = document.getElementById("aboutme");
  var digitalcv = document.getElementById("digitalcv");

  // get the current value of the clock's display property
  var displaySetting = aboutme.style.display;

  // now toggle the clock and the button text, depending on current state
  if (displaySetting == "none") {
    // clock is visible. hide it
    aboutme.style.display = "block";
    digitalcv.style.display = "none";
  }
}

function digitalcv() {
  // get the clock
  var aboutme = document.getElementById("aboutme");
  var digitalcv = document.getElementById("digitalcv");

  // get the current value of the clock's display property
  var displaySetting = digitalcv.style.display;

  // now toggle the clock and the button text, depending on current state
  if (displaySetting == "none") {
    // clock is visible. hide it
    digitalcv.style.display = "block";
    aboutme.style.display = "none";
  }
}

(function ($) {
  var $window = $(window),
    $body = $("body"),
    $wrapper = $("#wrapper"),
    $header = $("#header"),
    $nav = $("#nav"),
    $main = $("#main"),
    $navPanelToggle,
    $navPanel,
    $navPanelInner;

  // Breakpoints.
  breakpoints({
    default: ["1681px", null],
    xlarge: ["1281px", "1680px"],
    large: ["981px", "1280px"],
    medium: ["737px", "980px"],
    small: ["481px", "736px"],
    xsmall: ["361px", "480px"],
    xxsmall: [null, "360px"],
  });

  /**
   * Applies parallax scrolling to an element's background image.
   * @return {jQuery} jQuery object.
   */
  $.fn._parallax = function (intensity) {
    var $window = $(window),
      $this = $(this);

    if (this.length == 0 || intensity === 0) return $this;

    if (this.length > 1) {
      for (var i = 0; i < this.length; i++) $(this[i])._parallax(intensity);

      return $this;
    }

    if (!intensity) intensity = 0.25;

    $this.each(function () {
      var $t = $(this),
        $bg = $('<div class="bg"></div>').appendTo($t),
        on,
        off;

      on = function () {
        $bg.removeClass("fixed").css("transform", "matrix(1,0,0,1,0,0)");

        $window.on("scroll._parallax", function () {
          var pos = parseInt($window.scrollTop()) - parseInt($t.position().top);

          $bg.css("transform", "matrix(1,0,0,1,0," + pos * intensity + ")");
        });
      };

      off = function () {
        $bg.addClass("fixed").css("transform", "none");

        $window.off("scroll._parallax");
      };

      // Disable parallax on ..
      if (
        browser.name == "ie" || // IE
        browser.name == "edge" || // Edge
        window.devicePixelRatio > 1 || // Retina/HiDPI (= poor performance)
        browser.mobile
      )
        // Mobile devices
        off();
      // Enable everywhere else.
      else {
        breakpoints.on(">large", on);
        breakpoints.on("<=large", off);
      }
    });

    $window
      .off("load._parallax resize._parallax")
      .on("load._parallax resize._parallax", function () {
        $window.trigger("scroll");
      });

    return $(this);
  };

  // Play initial animations on page load.
  $window.on("load", function () {
    window.setTimeout(function () {
      $body.removeClass("is-preload");
    }, 100);
  });

  // Scrolly.
  $(".scrolly").scrolly();

  // Background.
  $wrapper._parallax(0.925);

  // Nav Panel.

  // Toggle.
  $navPanelToggle = $(
    '<a href="#navPanel" id="navPanelToggle">Menu</a>'
  ).appendTo($wrapper);

  // Change toggle styling once we've scrolled past the header.
  $header.scrollex({
    bottom: "5vh",
    enter: function () {
      $navPanelToggle.removeClass("alt");
    },
    leave: function () {
      $navPanelToggle.addClass("alt");
    },
  });

  // Panel.
  $navPanel = $(
    '<div id="navPanel">' +
      "<nav>" +
      "</nav>" +
      '<a href="#navPanel" class="close"></a>' +
      "</div>"
  )
    .appendTo($body)
    .panel({
      delay: 500,
      hideOnClick: true,
      hideOnSwipe: true,
      resetScroll: true,
      resetForms: true,
      side: "right",
      target: $body,
      visibleClass: "is-navPanel-visible",
    });

  // Get inner.
  $navPanelInner = $navPanel.children("nav");

  // Move nav content on breakpoint change.
  var $navContent = $nav.children();

  breakpoints.on(">medium", function () {
    // NavPanel -> Nav.
    $navContent.appendTo($nav);

    // Flip icon classes.
    $nav.find(".icons, .icon").removeClass("alt");
  });

  breakpoints.on("<=medium", function () {
    // Nav -> NavPanel.
    $navContent.appendTo($navPanelInner);

    // Flip icon classes.
    $navPanelInner.find(".icons, .icon").addClass("alt");
  });

  // Hack: Disable transitions on WP.
  if (browser.os == "wp" && browser.osVersion < 10)
    $navPanel.css("transition", "none");

  // Intro.
  var $intro = $("#intro");

  if ($intro.length > 0) {
    // Hack: Fix flex min-height on IE.
    if (browser.name == "ie") {
      $window
        .on("resize.ie-intro-fix", function () {
          var h = $intro.height();

          if (h > $window.height()) $intro.css("height", "auto");
          else $intro.css("height", h);
        })
        .trigger("resize.ie-intro-fix");
    }

    // Hide intro on scroll (> small).
    breakpoints.on(">small", function () {
      $main.unscrollex();

      $main.scrollex({
        mode: "bottom",
        top: "25vh",
        bottom: "-50vh",
        enter: function () {
          $intro.addClass("hidden");
        },
        leave: function () {
          $intro.removeClass("hidden");
        },
      });
    });

    // Hide intro on scroll (<= small).
    breakpoints.on("<=small", function () {
      $main.unscrollex();

      $main.scrollex({
        mode: "middle",
        top: "15vh",
        bottom: "-15vh",
        enter: function () {
          $intro.addClass("hidden");
        },
        leave: function () {
          $intro.removeClass("hidden");
        },
      });
    });
  }
})(jQuery);
