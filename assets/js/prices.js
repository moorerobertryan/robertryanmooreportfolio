function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

var price120GB = document.getElementById("120GB");
var price240GB = document.getElementById("240GB");
var price480GB = document.getElementById("480GB");

//put logic for getting the prices here

var priceList = JSON.parse(Get("http://127.0.0.1:5000/prices"));

price120GB.innerHTML = priceList.p120;
price240GB.innerHTML = priceList.p240;
price480GB.innerHTML = priceList.p480;


