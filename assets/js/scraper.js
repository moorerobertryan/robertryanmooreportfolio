function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

function CamelCaseToSpace(sentance){
    var sentanceArr = sentance.split(/(?=[A-Z])/);
    var sentance =  '';

    for(var k = 0; k < sentanceArr.length; k++){
        sentance += sentanceArr[k] + ' ';
    }

    return sentance;
}

Array.prototype.exclude = function(list){
    return this.filter(function(el){return list.indexOf(el)<0;})
}

//get JSON of starred projects
var starredProjects = JSON.parse(Get('https://api.github.com/users/robertryanmoore/starred'));
//get a list of featured project slots
var featuredProjectDescriptions = document.getElementsByClassName("featuredProjectDescription");
var featuredProjectHeadings = document.getElementsByClassName("featuredProjectHeading");
var featuredProjectLogos = document.getElementsByClassName("image fit");
var featuredProjectButtons = document.getElementsByClassName("button");
var featuredUL = document.getElementsByClassName("actions special");

for(var i = 0; i < featuredProjectDescriptions.length; i++){

    var currHeadingArr = starredProjects[i].name.split(/(?=[A-Z])/);
    var currHeading =  '';

    for(var k = 0; k < currHeadingArr.length; k++){
        currHeading += currHeadingArr[k] + ' ';
    }

    featuredProjectHeadings[i].innerHTML = currHeading;
    
    console.log(currHeading);

    //access the README and set its content as the discription
    var currDescription = Get('https://raw.githubusercontent.com/robertryanmoore/'+ starredProjects[i].name +'/master/README.md');
    featuredProjectDescriptions[i].innerHTML = currDescription;

    //set the image
    featuredProjectLogos[i].src = 'https://raw.githubusercontent.com/robertryanmoore/'+starredProjects[i].name+'/master/'+starredProjects[i].name+'.JPG';

    featuredUL[i].innerHTML = '<li><a  class="button">Source Code</a></li>';

    //set the link
    featuredProjectButtons[i].href = 'https://github.com/robertryanmoore/' + starredProjects[i].name;

}

// get other projects
var otherProjects = JSON.parse(Get('https://api.github.com/users/robertryanmoore/repos'));

otherProjects.exclude(starredProjects);

var projectsHTML = document.getElementById("otherProjects");

for(var i = 0; i < otherProjects.length; i++){
    if(starredProjects.includes(otherProjects[i])){
        console.log("true: " + otherProjects[i]);
    }
    projectsHTML.insertAdjacentHTML('afterbegin', "<article>\
    <header>\
      <h2 class=\"featuredProjectHeading\">"+CamelCaseToSpace(otherProjects[i].name)+"</h2>\
    </header>\
    <img src=\"\" alt=\"\" class=\"image fit\" />\
    <p class=\"featuredProjectDescription\">\
      Looks like we couldn't get the project information\
    </p>\
    <ul class=\"actions special\"></ul>\
  </article>");
}